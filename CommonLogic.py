def get_suggestions(query, count = -1):
    """Lấy danh sách suggestion của Elastic search dựa trên câu hỏi query nhập trên màn hình (Minh/Xuân)

    Parameters
    ----------
    query : str
        câu hỏi query nhập trên màn hình
    count : int
        số lượng câu hỏi muốn nhận
        -1 nghĩa là lấy hết

    Returns
    -------
    list [(str, [str, ...]), ...]
        danh sách các câu hỏi suggestion kèm với các category liên quan
    """

def get_question_categories(query):
    """Lấy danh sách category của câu hỏi query nhập trên màn hình (Minh/Xuân)

    Parameters
    ----------
    query : str
        câu hỏi query nhập trên màn hình

    Returns
    -------
    list [(str, ), ...]
        danh sách các câu hỏi suggestion
    """

def get_similaity_score(query, reference):
    """Tính điểm similarity giữa câu query nhập trên màn hình so với câu reference (Duyên)

    Parameters
    ----------
    query : str
        câu hỏi query nhập trên màn hình
    reference : str
        câu hỏi dùng để reference khi tính điểm

    Returns
    -------
    float
        điểm similarity giữa câu query nhập trên màn hình so với câu reference
    """

def get_keywords_in_category(category):
    """Lấy danh sách các keyword và tần suất xuất hiện trong category (Minh/Xuân)

    Parameters
    ----------
    category : str
        category để lấy keywords

    Returns
    -------
    list [(str, int), ...]
        danh sách keyword và tần suất xuất hiện trong câu query [(keyword, count), ...]
    """

def get_questions_from_keyword(keyword, count = -1):
    """Lấy danh sách các câu hỏi liên quan đến keyword (Optional)

    Parameters
    ----------
    keyword : str
        từ keyword
    count : int
        số lượng câu hỏi muốn nhận
        -1 nghĩa là lấy hết

    Returns
    -------
    list [(str, [str, ...]), ...]
        danh sách các câu hỏi suggestion kèm với các category liên quan
    """
